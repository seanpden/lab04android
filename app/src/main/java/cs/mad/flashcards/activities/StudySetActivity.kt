package cs.mad.flashcards.activities

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.databinding.StudySetBinding
import cs.mad.flashcards.entities.Flashcard


class StudySetActivity : AppCompatActivity() {
    // main variable section
    private lateinit var binding: StudySetBinding
    private val flashcards = Flashcard.getHardcodedFlashcards().toMutableList()
    private var isClicked = false
    private var missedCount = 0
    private var correctCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = StudySetBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initView()

        // flip card listener
        binding.cardView.setOnClickListener{
            flipCard()
        }

        // exit
        binding.exitButton.setOnClickListener{
            startActivity(Intent(this, FlashcardSetDetailActivity::class.java))
        }

        // missed
        binding.missed.setOnClickListener{
            // store 'missed' flashcard at end of flashcard list
            val thisOne = flashcards.removeFirst()
            flashcards.add(thisOne)
            missedCount += 1
            initView()
        }

        // skip
        binding.skip.setOnClickListener{
            val thisOne = flashcards.removeFirst()
            flashcards.add(thisOne)
            initView()
        }

        // correct
        binding.correct.setOnClickListener{
            val thisOne = flashcards.removeFirst()
            correctCount += 1
            if (flashcards.size == 0) {
                AlertDialog.Builder(it.context)
                    .setTitle("Finished")
                    .setPositiveButton("ez") { _,_ -> }
                    .create()
                    .show()
//                startActivity(Intent(this, FlashcardSetDetailActivity::class.java))
            }
            else {
                initView()
            }
        }

    }

    private fun initView() {
        // initalizes view and updates view once past a card
        if (isClicked) {
            binding.flashcardSetTitle.text = flashcards[0].answer
        }
        else {
            binding.flashcardSetTitle.text = flashcards[0].question
        }

    }

    private fun flipCard() {

        if (flashcards.isNotEmpty()) {
            isClicked = !isClicked
            initView()
        }

    }

}
package cs.mad.flashcards.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.entities.Flashcard

class FlashcardAdapter(dataSet: List<Flashcard>) :
    RecyclerView.Adapter<FlashcardAdapter.ViewHolder>() {

    private val dataSet = dataSet.toMutableList()

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val flashcardTitle: TextView = view.findViewById(R.id.flashcard_title)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcard, viewGroup, false)
        )
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = dataSet[position]
        viewHolder.flashcardTitle.text = item.question

//        On click of term, shows dialogue of term and definition with buttons (done, edit, delete)
        viewHolder.itemView.setOnClickListener {

            AlertDialog.Builder(it.context)
                .setTitle(item.question)
                .setMessage(item.answer)
                .setPositiveButton("ez") { _,_ -> }
                .setNeutralButton("edit") { _, _ -> editDial(it.context, item)}
                .create()
                .show()
        }
    }

    private fun editDial(context: Context, flashcard: Flashcard) {

        val customTitle = EditText(context)
        val customBody = EditText(context)
        customTitle.setText(flashcard.question)
        customBody.setText(flashcard.answer)

        AlertDialog.Builder(context)
            .setCustomTitle(customTitle)
            .setView(customBody)
            .setPositiveButton("done") {_,_ ->}
            .setNegativeButton("delete") {_,_ ->}
            .create()
            .show()
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun addItem(it: Flashcard) {
        dataSet.add(it)
        notifyItemInserted(dataSet.lastIndex)
    }
}